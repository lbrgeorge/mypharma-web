import React, { Component } from 'react';
import axios from 'axios';

export default class UploadComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {selected_file: undefined};
    }

    componentDidMount() {
        this.state = {selected_file: undefined};
    }

    fileSelected(e) {
        this.setState({selected_file: e.target.files[0]});
    }

    uploadFile() {
        if (this.state.selected_file != undefined) {
            var data = new FormData();
            data.append('image', this.state.selected_file);

            var config = {
                mode: 'no-cors',
                onUploadProgress: function(event) {
                    var percentage = Math.round((event.loaded * 100) / event.total);
                    console.log(percentage);
                }
            }

            //Started uploading callback
            this.props.onUploadStart();

            axios.post(`http://${window.location.hostname}:8182/upload`, data, config).then((res) => {
                this.props.onUploadComplete(res.data);
            });
        }
    }

    render() {
        return (
            <div className="input-group">
                <div className="form-control cover-input" onClick={() => this.upload_image.click()}>{this.state.selected_file == undefined ? "Selecione uma imagem" : this.state.selected_file.name}</div>
                <input type="file" id="image" accept="image/*" style={{display: "none"}} onChange={(e) => this.fileSelected(e)} ref={(refs) => this.upload_image = refs} />
                <div className="input-group-append">
                    <button className="btn btn-lg btn-secondary" type="button" onClick={() => this.uploadFile()}>Enviar</button>
                </div>
            </div>
        )
    }
};

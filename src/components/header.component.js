import React, { Component } from 'react';

export default class HeaderComponent extends Component {
    render() {
        return (
            <header className="masthead mb-auto">
                <div className="inner">
                    <div className="masthead-brand">
                        <h3>MyPharma</h3>
                    </div>
                </div>
            </header>
        )
    }
}
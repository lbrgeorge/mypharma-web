import React, { Component, Fragment } from 'react';
import UploadComponent from './upload.component';

/*
  * Thank you!
  * http://stackoverflow.com/questions/5598743/finding-elements-position-relative-to-the-document#answer-26230989
*/
function getCoords(elem) { // crossbrowser version
  var box = elem.getBoundingClientRect();

  var body = document.body;
  var docEl = document.documentElement;

  var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
  var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

  var clientTop = docEl.clientTop || body.clientTop || 0;
  var clientLeft = docEl.clientLeft || body.clientLeft || 0;

  var top = box.top + scrollTop - clientTop;
  var left = box.left + scrollLeft - clientLeft;

  return {top: Math.round(top), left: Math.round(left)};
}

export default class HomeComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {rects: [], faces: [], image: "", isLoading: false};
  }

  componentDidMount() {
    //Let's fix the rects position if the window has changed the size
    window.addEventListener('resize', () => {
      this.renderRects();
    })
  }

  uploadCompleted(data) {
    this.setState({faces: data.faces, image: `http://${window.location.hostname}:8182/image/${data.image}`, isLoading: false});
  }

  renderRects() {
    //Get ration based on img bounding and the real image width
    var ratio = this.current_image.getBoundingClientRect().width / this.current_image.naturalWidth;
    var coords = getCoords(this.current_image);

    this.setState({rects: []});

    this.state.faces.forEach(f => {
      this.setState((state) => {
        var border = "8px solid white";
        if (f.gender.gender == "MALE") border = "8px solid #4189f4";
        if (f.gender.gender == "FEMALE") border = "8px solid #f442bf";

        state.rects.push({
          width: (f.face_location.width * ratio) + "px",
          height: (f.face_location.height * ratio) + "px",
          left: (coords.left + f.face_location.left * ratio) + 'px',
          top: (coords.top + f.face_location.top * ratio) + 'px',
          border: border
        });

        return state;
      });
    });
  }

  render() {
    return (
      <main role="main" className="inner cover">
        <div className="image-result" id="image-result" ref={(refs) => this.image_result = refs}>
          {
            this.state.isLoading == true ? (
              <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><g transform="translate(50,50)">
                <g transform="scale(0.2)">
                <g transform="translate(-50,-50)">
                <g transform="rotate(280.606 50 50)">
                  <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" values="360 50 50;240 50 50;120 50 50;0 50 50" keyTimes="0;0.333;0.667;1" dur="1s" keySplines="0.7 0 0.3 1;0.7 0 0.3 1;0.7 0 0.3 1" calcMode="spline"></animateTransform>
                  <path fill="#93dbe9" d="M54.3,28.1h34.2c-4.5-9.3-12.4-16.7-21.9-20.8L45.7,28.1L54.3,28.1L54.3,28.1z"></path>
                  <path fill="#689cc5" d="M61.7,7.3C51.9,4,41.1,4.2,31.5,8.1v29.5l6.1-6.1L61.7,7.3C61.7,7.3,61.7,7.3,61.7,7.3z"></path>
                  <path fill="#5e6fa3" d="M28.1,11.6c-9.3,4.5-16.7,12.4-20.8,21.9l20.8,20.8v-8.6L28.1,11.6C28.1,11.6,28.1,11.6,28.1,11.6z"></path>
                  <path fill="#3b4368" d="M31.5,62.4L7.3,38.3c0,0,0,0,0,0C4,48.1,4.2,58.9,8.1,68.5h29.5L31.5,62.4z"></path>
                  <path fill="#93dbe9" d="M45.7,71.9H11.5c0,0,0,0,0,0c4.5,9.3,12.4,16.7,21.9,20.8l20.8-20.8H45.7z"></path>
                  <path fill="#689cc5" d="M62.4,68.5L38.3,92.6c0,0,0,0,0,0c9.8,3.4,20.6,3.1,30.2-0.8V62.4L62.4,68.5z"></path>
                  <path fill="#5e6fa3" d="M71.9,45.7v8.6v34.2c0,0,0,0,0,0c9.3-4.5,16.7-12.4,20.8-21.9L71.9,45.7z"></path>
                  <path fill="#3b4368" d="M91.9,31.5C91.9,31.5,91.9,31.5,91.9,31.5l-29.5,0l0,0l6.1,6.1l24.1,24.1c0,0,0,0,0,0 C96,51.9,95.8,41.1,91.9,31.5z"></path>
                </g></g></g></g>
              </svg>
            ) : (
              <Fragment>
                <img src={this.state.image} alt="" onLoad={() => this.renderRects()} ref={(refs) => this.current_image = refs} />
                {this.state.rects.map((item, index) => {
                  return <div className="rect" key={index} style={item}></div>;
                })}
              </Fragment>
            )
          }
        </div>

        <div className="lead">
            <UploadComponent onUploadComplete={(data) => this.uploadCompleted(data)} onUploadStart={() => this.setState({isLoading: true})} />
        </div>
      </main>
    )
  }
};
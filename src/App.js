import React, { Component } from 'react';

//Components
import HeaderComponent from './components/header.component';
import HomeComponent from './components/home.component';
import FooterComponent from './components/footer.component';

class App extends Component {
  render() {
    return (
      <div className="app">
        <div className="text-center cover-container d-flex h-100 p-3 mx-auto flex-column">
          <HeaderComponent />
          <HomeComponent />
          <FooterComponent />
        </div>
      </div>
    );
  }
}

export default App;

# MyPharma Web
Aplicativo web para reconhecimento facial. Este aplicativo foi feito com React e com Watson Visual Recognition.

Para iniciar o aplicativo é necessário instalar as dependências com:
`npm install`

Após isso, basta executar: 
`npm start`

**OBS:** Para o funcionamento deste aplicativo é preciso iniciar o servidor.
O servidor está disponível [aqui](https://bitbucket.org/lbrgeorge/mypharma-server).
